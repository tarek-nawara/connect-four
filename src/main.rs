#[macro_use]
extern crate log;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate simple_logger;
extern crate term;
#[macro_use]
extern crate text_io;

mod game;
use game::game_io;
use std::fs::File;
use game::entities::Game;
use game::entities::GameBuilder;
use game::entities::GameState;
use game::score_board::HistoryBoard;
use game::score_board::ScoreRecord;
use game::parser::Configuration;
use std::io::Read;
use std::io::BufReader;

const UNDO_KEY:i32 = -1;
const REDO_KEY:i32 = -2;
const SAVE:i32 = -3;
const PRINT_HISTORY:i32 = -4;

static LOGO_PATH: &'static str = "config/logo.txt";
static CONFIG_PATH: &'static str = "config/configurations.xml";
static HISTORY_BOARD_PATH: &'static str = "config/history.txt";

fn main() {
    simple_logger::init().unwrap();
    let mut terminal = term::stdout().unwrap();
    terminal.fg(term::color::BRIGHT_CYAN).unwrap();
    print_logo();
    terminal.reset().unwrap();

    let configuration = game::parser::read_configurations(CONFIG_PATH);
    info!("Configurations={:?}", configuration);
    let mut game_world = create_game(&configuration);
    loop {
        game_world.pretty_print();
        match game_world.state {
            GameState::PlayerOneTurn => println!("{} is move", game_world.player_one.name),
            GameState::PlayerTwoTurn => println!("{} is move", game_world.player_two.name),
        }
        let col: i32 = read!();
        if col == SAVE {
            game_io::serialize_game(&game_world);
            info!("[main]---Game world saved successfully");
        } else if col == UNDO_KEY {
            game_world.undo();
        } else if col == REDO_KEY {
            game_world.redo();
        } else if col == PRINT_HISTORY {
            let history_board = HistoryBoard::load(HISTORY_BOARD_PATH, configuration.high_scores);
            history_board.pretty_print();
        }
        else if !game_world.play(col as usize) {
            println!("Invalid move");
        }
        
        if game_world.is_game_over() { break; }
    }
    
    game_world.pretty_print();
    let score_record_one = ScoreRecord {
        player_name: String::from(game_world.player_one.name),
        player_score: game_world.player_one.score,
    };

    let score_record_two = ScoreRecord {
        player_name: String::from(game_world.player_two.name),
        player_score: game_world.player_two.score,
    };
    
    let mut history_board = HistoryBoard::load(HISTORY_BOARD_PATH, configuration.high_scores);
    history_board.add_score(score_record_one);
    history_board.add_score(score_record_two);
    history_board.pretty_print();
    history_board.save(HISTORY_BOARD_PATH);
}

fn create_game(configuration: &Configuration) -> Game {
    println!("1. Load Game\n2. New Game");
    let choice: i32 = read!();
    if choice == 1 {
        game::game_io::load_game()
    } else {

        let name_one = read_player_info();
        let symbol_one = read_player_symbol();
        let name_two = read_player_info();
        let symbol_two = if symbol_one == 'x' { 'o' } else { 'x' };

        GameBuilder::new()
            .width(configuration.width)
            .height(configuration.height)
            .player_one_info((&name_one, symbol_one))
            .player_two_info((&name_two, symbol_two))
            .build()
    }
}

fn print_logo() -> () {
    let file = File::open(LOGO_PATH).expect("Failed to open logo file");
    let mut buf_reader = BufReader::new(file);
    let mut logo = String::new();
    buf_reader
        .read_to_string(&mut logo)
        .expect("Failed to read logo");
    println!("\t\t{}", logo);
}

fn read_player_info() -> String {
    println!("Enter player name");
    let name: String = read!();
    return name;
}

fn read_player_symbol() -> char {
    loop {
        println!("Choose symbol:\n1. x\n2. o");
        let choice: i32 = read!();
        if choice == 1 {
            return 'x';
        } else if choice == 2 {
            return 'o';
        } else {
            println!("Invalid choice");
        }
    }
}
