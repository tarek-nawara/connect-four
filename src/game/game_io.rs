use game::entities::Game;
use serde_json;

use std::fs::File;
use std::io::{BufReader, BufWriter, Write};

static FILE_NAME: &'static str = "saved_games/game.json";

pub fn serialize_game(game_world: &Game) -> () {
    let serialized = serde_json::to_string(game_world).unwrap();
    let file = File::create(FILE_NAME).expect("Error couldn't create the file");
    let mut buf_writer = BufWriter::new(file);
    buf_writer
        .write_all(serialized.as_bytes())
        .expect("Failed to write game");
}

pub fn load_game() -> Game {
    let file = File::open(FILE_NAME).expect("Error failed to open the file");
    let buf_reader = BufReader::new(file);
    serde_json::from_reader(buf_reader).unwrap()
}
