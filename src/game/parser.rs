use std::fs::File;
use std::io::BufReader;
use super::xml::reader::{EventReader, XmlEvent};

#[derive(Debug)]
pub struct Configuration {
    pub width: usize,
    pub height: usize,
    pub high_scores: usize,
}

impl Configuration {
    pub fn new() -> Configuration {
        Configuration {
            width: 8,
            height: 8,
            high_scores: 5,
        }
    }
}

pub fn read_configurations(filename: &str) -> Configuration {
    let file = File::open(filename).unwrap();
    let buf_reader = BufReader::new(file);

    let parser = EventReader::new(buf_reader);
    let mut configuration = Configuration::new();
    let mut tag = String::from("");
    for e in parser {
        match e {
            Ok(XmlEvent::StartElement { name, .. }) => {
                tag = name.local_name;
            }
            Ok(XmlEvent::Characters(data)) => {
                assign_value(&mut configuration, &tag, data.parse::<usize>().unwrap());
            }
            Err(e) => {
                warn!("Error: {}", e);
                break;
            }
            _ => {}
        }
    }
    return configuration;
}

fn assign_value(configuration: &mut Configuration, field_name: &str, value: usize) -> () {
    match field_name {
        "Width" => {
            configuration.width = value;
        }
        "Height" => {
            configuration.height = value;
        }
        "Highscores" => {
            configuration.high_scores;
        }
        _ => warn!("Error: unsupported xml filed"),
    }
}
