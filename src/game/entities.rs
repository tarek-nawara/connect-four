extern crate serde_json;

type Symbol = char;

/// Representation of the game world
#[derive(Debug, Serialize, Deserialize)]
pub struct Game {
    pub width: usize,
    pub height: usize,
    pub board: Vec<Vec<Symbol>>,
    pub player_one: Player,
    pub player_two: Player,
    pub state: GameState,
    undo_stack: Vec<Move>,
    redo_stack: Vec<Move>,
    column_status: Vec<i32>,
    full_columns: usize,
}

/// `GameState` struct is used to 
///  switch between players
#[derive(Debug, Serialize, Deserialize)]
pub enum GameState {
    PlayerOneTurn,
    PlayerTwoTurn,
}

/// Builder for the game
pub struct GameBuilder<'a> {
    width: usize,
    height: usize,
    player_one_info: (&'a str, char),
    player_two_info: (&'a str, char),
}

/// Representation of the player
#[derive(Debug, Serialize, Deserialize)]
pub struct Player {
    pub symbol: char,
    pub name: String,
    pub score: i32,
}

/// Representation of the game move
/// each move contains the player's symbol
/// who commited the move and the scores
/// for each player after that move.
#[derive(Debug, Serialize, Deserialize)]
pub struct Move {
    player_symbol: Symbol,
    location: (usize, usize),
    player_one_score: i32,
    player_two_score: i32,
}

impl<'a> GameBuilder<'a> {
    pub fn new() -> GameBuilder<'a> {
        GameBuilder {
            width: 0usize,
            height: 0usize,
            player_one_info: ("player_one", 'x'),
            player_two_info: ("player_two", 'o'),
        }
    }

    pub fn width(&mut self, width: usize) -> &mut GameBuilder<'a> {
        self.width = width;
        self
    }

    pub fn height(&mut self, height: usize) -> &mut GameBuilder<'a> {
        self.height = height;
        self
    }

    pub fn player_one_info(&mut self, info: (&'a str, char)) -> &mut GameBuilder<'a> {
        self.player_one_info = info;
        self
    }

    pub fn player_two_info(&mut self, info: (&'a str, char)) -> &mut GameBuilder<'a> {
        self.player_two_info = info;
        self
    }

    pub fn build(&self) -> Game {
        Game {
            width: self.width,
            height: self.height,
            board: vec![vec![' '; self.width]; self.height],
            player_one: Player {
                symbol: self.player_one_info.1,
                name: String::from(self.player_one_info.0),
                score: 0,
            },
            player_two: Player {
                symbol: self.player_two_info.1,
                name: String::from(self.player_two_info.0),
                score: 0,
            },
            undo_stack: Vec::new(),
            redo_stack: Vec::new(),
            state: GameState::PlayerOneTurn,
            column_status: vec![self.height as i32 - 1; self.width],
            full_columns: 0,
        }
    }
}

impl Game {
    /// Responsible for printing the game
    /// in the console.
    pub fn pretty_print(&self) -> () {
        for _ in 0..self.width {
            print!("----");
        }
        println!("");
        for i in 0..self.height {
            for j in 0..self.width {
                if j == 0 {
                    print!("| ");
                }
                print!("{} | ", self.board[i][j]);
            }
            println!("");
            for _ in 0..self.width {
                print!("----");
            }
            println!("");
        }
        println!(
            "Player one: {}, symbol: {}, score: {}",
            self.player_one.name,
            self.player_one.symbol,
            self.player_one.score
        );
        println!(
            "Player two: {}, symbol: {}, score: {}",
            self.player_two.name,
            self.player_two.symbol,
            self.player_two.score
        );
    }

    /// given a column number, this fn will
    /// first check if the column has empty slot
    /// and if this is the case will fill it with
    /// the right player's symbol and update the scores.
    pub fn play(&mut self, col: usize) -> bool {
        if col >= self.width || self.column_status[col] < 0 {
            false
        } else {
            let (symbol, next_state) = match self.state {
                GameState::PlayerOneTurn => (self.player_one.symbol, GameState::PlayerTwoTurn),
                GameState::PlayerTwoTurn => (self.player_two.symbol, GameState::PlayerOneTurn),
            };
            self.board[self.column_status[col] as usize][col] = symbol;
            
            let location = (self.column_status[col], col as i32);
            self.column_status[col] -= 1;
            if self.column_status[col] < 0 {
                self.full_columns += 1;
            }
            let added_score = self.get_added_score(location, symbol);
            match self.state {
                GameState::PlayerOneTurn => self.player_one.score += added_score,
                GameState::PlayerTwoTurn => self.player_two.score += added_score,
            };
            self.state = next_state;

            let player_move = Move {
                player_symbol: symbol,
                location: (location.0 as usize, location.1 as usize),
                player_one_score: self.player_one.score,
                player_two_score: self.player_two.score,
            };
            self.undo_stack.push(player_move);
            self.redo_stack.clear();
            
            true
        }
    }

    /// Undo the last commited move.
    pub fn undo(&mut self) -> () {
        if self.undo_stack.is_empty() {
            warn!("[undo]---Can't undo");
            return;
        }
        let last_move = self.undo_stack.pop().unwrap();
        let (location_x, location_y) = last_move.location;
        if self.column_status[location_y] == 0 {
            self.full_columns -= 1;
        }
        self.column_status[location_y] += 1;
        self.board[location_x][location_y] = ' ';
        if self.undo_stack.is_empty() {
            self.player_one.score = 0;
            self.player_two.score = 0;
        } else {
            let pre_last_move = self.undo_stack.last().unwrap();
            self.player_one.score = pre_last_move.player_one_score;
            self.player_two.score = pre_last_move.player_two_score;
        }
        self.redo_stack.push(last_move);
    }

    /// redo the last commited move.
    pub fn redo(&mut self) -> () {
        if self.redo_stack.is_empty() {
            warn!("[redo]---Can't redo");
            return;
        }
        let last_move = self.redo_stack.pop().unwrap();
        let (location_x, location_y) = last_move.location;
        self.column_status[location_y] -= 1;
        if self.column_status[location_y] == 0 {
            self.full_columns += 1;
        }
        self.board[location_x][location_y] = last_move.player_symbol;
        self.player_one.score = last_move.player_one_score;
        self.player_two.score = last_move.player_two_score;
        self.undo_stack.push(last_move);
    }

    /// Returns true if the game has ended.
    pub fn is_game_over(&self) -> bool {
        self.full_columns == self.width
    }

    fn get_added_score(&self, location: (i32, i32), symbol: Symbol) -> i32 {
        info!("[get_added_score]---Location: {:?}", location);
        let mut added_score = 0;
        added_score += self.update_row(location, symbol);
        added_score += self.update_col(location, symbol);
        added_score += self.update_diagonal(location, symbol);
        added_score
    }

    fn update_row(&self, location: (i32, i32), target_symbol: Symbol) -> i32 {
        info!("[update_row]---Start updating row score");
        let (row, col) = location;
        let cuts: Vec<Vec<(i32, i32)>> = (row..row + 4)
            .map(|new_row| {
                vec![
                    (new_row, col),
                    (new_row - 1, col),
                    (new_row - 2, col),
                    (new_row - 3, col),
                ]
            })
            .collect();
        let mut sum = 0;
        for v in cuts {
            sum += self.is_four_connected(v, target_symbol);
        }
        sum
    }

    fn update_col(&self, location: (i32, i32), target_symbol: Symbol) -> i32 {
        info!("[update_col]---Start updating column score");
        let (row, col) = location;
        let cuts: Vec<Vec<(i32, i32)>> = (col..col + 4)
            .map(|new_col| {
                vec![
                    (row, new_col),
                    (row, new_col - 1),
                    (row, new_col - 2),
                    (row, new_col - 3),
                ]
            })
            .collect();
        let mut sum = 0;
        for v in cuts {
            sum += self.is_four_connected(v, target_symbol);
        }
        sum
    }

    fn update_diagonal(&self, location: (i32, i32), target_symbol: Symbol) -> i32 {
        info!("[update_diagonal]---Start updating diagonal");
        let (row, col) = location;
        let mut sum = 0;
        for delta in 0..4 {
            let (new_row, new_col) = (row + delta, col - delta);
            let cut = vec![
                (new_row, new_col),
                (new_row - 1, new_col + 1),
                (new_row - 2, new_col + 2),
                (new_row - 3, new_col + 3),
            ];
            sum += self.is_four_connected(cut, target_symbol);
        }

        for delta in 0..4 {
            let (new_row, new_col) = (row + delta, col + delta);
            let cut = vec![
                (new_row, new_col),
                (new_row - 1, new_col - 1),
                (new_row - 2, new_col - 2),
                (new_row - 3, new_col - 3),
            ];
            sum += self.is_four_connected(cut, target_symbol);
        }
        sum
    }

    fn is_four_connected(&self, points: Vec<(i32, i32)>, target_symbol: Symbol) -> i32 {
        let connected = points
            .iter()
            .filter(|&&(x, y)| {
                x >= 0 && x < self.height as i32 && y >= 0 && y < self.width as i32
            })
            .filter(|&&(x, y)| {
                self.board[x as usize][y as usize] == target_symbol
            })
            .count();

        if connected == 4 {
            1
        } else {
            0
        }
    }
}
