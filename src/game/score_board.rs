
use std::io::BufReader;
use std::io::BufRead;
use std::io::Write;
use std::io::BufWriter;
use std::fs::File;

pub struct HistoryBoard {
    history: Vec<ScoreRecord>,
    max_records: usize,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
pub struct ScoreRecord {
    pub player_name: String,
    pub player_score: i32,
}

impl HistoryBoard {
    /// Used to read the history from a file
    pub fn load(filename: &str, max_records: usize) -> HistoryBoard {
        let file = File::open(filename).unwrap();
        let reader = BufReader::new(&file);
        let mut history_board = HistoryBoard { history: Vec::new(), max_records: max_records };
        for line in reader.lines() {
            history_board.history.push(HistoryBoard::parse_score_record(&line.unwrap()));
        }
        info!("[load]---Score history loaded");
        return history_board;
    }

    /// Used to save the history in a file
    pub fn save(&self, filename: &str) -> () {
        let file = File::create(filename).unwrap();
        let mut writer = BufWriter::new(file);
        for score_record in &(self.history) {
            writer.write(&(score_record.player_name).as_bytes());
            writer.write(b" ");
            writer.write(score_record.player_score.to_string().as_bytes());
            writer.write(b"\n");
        }
        info!("[save]---Scores saved");
    }

    /// Used for pretty printing the score board
    /// in the console after the game is finished.
    pub fn pretty_print(&self) -> () {
        println!("------------Score board-----------");
        for score_record in &(self.history) {
            println!("Player name:{}, Player score:{}",
                     score_record.player_name, score_record.player_score);
        }
    }

    /// Used to add elements to the history list
    /// the history list will only keep at max the
    /// max score records.
    pub fn add_score(&mut self, score_record: ScoreRecord) -> () {
        self.history.push(score_record);
        self.history.sort();
        if self.history.len() > self.max_records {
            self.history.pop();
        }
    }

    fn parse_score_record(line: &str) -> ScoreRecord {
        let tokens: Vec<&str> = line.split(" ").collect();
        ScoreRecord {
            player_name: String::from(tokens[0]),
            player_score: tokens[1].parse::<i32>().unwrap(),
        }
    }
}
